#!/usr/bin/env python3
"""
Unmarshall XML and serialize it to JSON.
"""

# Standard library imports
from __future__ import annotations
import argparse
import json

# Third-party library imports
import xmltodict


EXAMPLE = """
Example:
    xml2json < input.xml > output.json

    OR

    xml2json < input.xml | jq -c ".[]"
"""


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description=__doc__, epilog=EXAMPLE)
    parser.add_argument(
        "-i",
        "--input",
        default="-",
        type=argparse.FileType("r"),
        metavar=".XML",
        help="input XML file"
    )
    parser.add_argument(
        "-o",
        "--output",
        default="-",
        type=argparse.FileType("w"),
        metavar=".JSON",
        help="output JSON file"
    )
    args = parser.parse_args()
    return args


def main() -> None:
    args = parse_args()
    args.output.write(
        json.dumps(
            xmltodict.parse(
                args.input.read()
            )
        )
    )


if __name__ == "__main__":
    main()
